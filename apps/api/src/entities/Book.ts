import { Field, ID, ObjectType } from 'type-graphql'
import { Entity, PrimaryKey, Property } from '@mikro-orm/core'

@ObjectType({ description: 'The Book model' })
@Entity()
export class Book {
    @Field(() => ID)
    @PrimaryKey()
    id!: number

    @Property({ onCreate: () => new Date() })
    createdAt: Date = new Date()

    @Property({ onUpdate: () => new Date() })
    updatedAt: Date = new Date()

    @Field()
    @Property()
    name!: string

    @Field()
    @Property()
    resume!: string

    @Field()
    @Property({ columnType: 'double' })
    note!: number

    constructor(name: string, resume: string, note: number) {
        this.name = name
        this.resume = resume
        this.note = note
    }
}
