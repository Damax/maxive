import express from 'express'
import { MikroORM } from '@mikro-orm/core'
import { graphqlHTTP } from 'express-graphql'
import { buildSchema } from 'type-graphql'
import { IServerOptions } from './types/server.models'
import { TsMorphMetadataProvider } from '@mikro-orm/reflection'

import { BookResolver } from './resolvers/Book'
import mikroOrmConfig from './mikro-orm.config'

export class Server {
    private app: express.Application
    private options: IServerOptions = {
        endpoint: '/graphql',
        port: 4000,
        host: '0.0.0.0',
    }

    constructor(options?: IServerOptions) {
        this.app = express()

        if (options) {
            Object.assign(this.options, options)
        }
    }

    public get url(): string {
        return `http://${this.options.host}:${this.options.port}${this.options.endpoint}`
    }

    public async run() {
        const orm = await MikroORM.init(
            Object.assign(mikroOrmConfig, {
                metadataProvider: TsMorphMetadataProvider,
            })
        )

        const schema = await buildSchema({
            resolvers: [BookResolver],
            validate: false,
        })

        this.app.use(
            this.options.endpoint,
            graphqlHTTP({
                schema,
                graphiql: true,
                context: {
                    entityManager: orm.em.fork(),
                },
            })
        )

        this.app.listen({ port: this.options.port }, () => {
            console.log(`🚀 Server ready and listening at ==> ${this.url}`)
        })
    }
}
