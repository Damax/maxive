import { Server } from './server'

async function main() {
    const server = new Server()
    try {
        await server.run()
    } catch (error) {
        console.log(error)
    }
}

main()
