import { MikroORM, EntityRepository } from '@mikro-orm/core'

import { Book } from '../entities/Book'
import { Arg, Ctx, Mutation, Query, Resolver } from 'type-graphql'
import { BookInput } from './types/book-input'
import { IContextManager } from 'types'

@Resolver()
export class BookResolver {
    @Query((_returns) => Book, { nullable: false })
    async getBook(
        @Arg('id') id: number,
        @Ctx() { entityManager }: IContextManager
    ) {
        return entityManager.findOneOrFail(Book, { id })
    }

    @Query(() => [Book])
    async allBooks(@Ctx() { entityManager }: IContextManager) {
        const entity = entityManager.getRepository(Book)
        return entity.findAll()
    }

    @Mutation(() => Book)
    async createBook(
        @Arg('data') data: BookInput,
        @Ctx() { entityManager }: IContextManager
    ): Promise<Book> {
        const book = new Book(data.name, data.resume, data.note)

        try {
            await entityManager.persistAndFlush(book)
        } catch (error) {
            console.log(error)
            throw new Error('The book can’t be save.')
        }

        return book
    }
}
