import { Book } from '../../entities/Book'
import { Field, InputType } from 'type-graphql'
import { Length } from 'class-validator'

@InputType()
export class BookInput implements Partial<Book> {
    @Field()
    name!: string

    @Field()
    @Length(1, 500)
    resume!: string

    @Field()
    note!: number
}
