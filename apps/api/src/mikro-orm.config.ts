import { MikroORMOptions } from '@mikro-orm/core'

export default {
    entities: ['./build/entities/**/*.js'],
    entitiesTs: ['./src/entities/**/*.ts'],
    type: 'mariadb',
    dbName: process.env['MYSQL_DATABASE'] ?? 'maxive',
    password: process.env['MYSQL_PASSWORD'] ?? 'root',
    user: process.env['MYSQL_USER'] ?? 'root',
    port: process.env['MYSQL_PORT'] ?? 3306,
    host: process.env['MYSQL_HOST'] ?? '0.0.0.0',
} as MikroORMOptions
