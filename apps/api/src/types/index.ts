import { EntityManager } from '@mikro-orm/mariadb'

export interface IContextManager {
    entityManager: EntityManager
}
