export interface IServerOptions {
    endpoint: string
    port?: number
    host?: string
}
