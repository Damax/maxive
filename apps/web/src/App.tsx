import React from 'react'
import { MainSection } from './components/MainSection'
import { Footer } from './components/partials/Footer'

class App extends React.Component {
    render(): React.ReactNode {
        return (
            <div className="app">
                <main>
                    <h1>Maxive</h1>
                    <MainSection />
                </main>
                <Footer />
            </div>
        )
    }
}

export default App
