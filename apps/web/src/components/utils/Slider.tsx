import React, { useRef } from 'react'
import { SliderItem, SliderItemProps } from './SliderItem'

type SliderState = { items: SliderItemProps[] }

export class Slider extends React.Component<{}, SliderState> {
    constructor(props: {}) {
        super(props)
        this.state = {
            items: [
                { key: 1, next: 2, active: false },
                { key: 2, next: 1, active: false },
                { key: 3, active: true },
                { key: 4, prev: 1, active: false },
                { key: 5, prev: 2, active: false },
            ],
        } as { items: SliderItemProps[] }
    }

    private goNext(): void {
        this.setState((state: SliderState) => ({
            items: state.items.map((value: SliderItemProps) => {
                let res: Partial<SliderItemProps> = { ...value }
                if (value.active) {
                    res = {
                        active: false,
                        next: 1,
                    }
                } else if (value.prev === 1) {
                    res = {
                        active: true,
                        prev: undefined,
                        next: undefined,
                    }
                } else if (value.next) {
                    res = {
                        next: ++value.next,
                    }
                } else if (value.prev) {
                    res = {
                        prev: --value.prev,
                        active: false,
                    }
                }
                return Object.assign({ ...value }, res)
            }),
        }))
    }

    private goPrev(): void {
        this.setState((state: SliderState) => ({
            items: state.items.map((value: SliderItemProps) => {
                let res: Partial<SliderItemProps> = { ...value }
                if (value.active) {
                    res = {
                        active: false,
                        prev: 1,
                    }
                } else if (value.next === 1) {
                    res = {
                        active: true,
                        prev: undefined,
                        next: undefined,
                    }
                } else if (value.prev) {
                    res = {
                        prev: value.prev + 1,
                    }
                } else if (value.next) {
                    res = {
                        next: value.next - 1,
                    }
                }
                return Object.assign({ ...value }, res)
            }),
        }))
    }

    render(): React.ReactNode {
        return (
            <section className="slider">
                <span
                    className="slider__arrow-left"
                    onClick={this.goPrev.bind(this)}
                ></span>
                <span
                    className="slider__arrow-right"
                    onClick={this.goNext.bind(this)}
                ></span>
                <div className="slider__items">
                    {this.state.items.map((item) => (
                        <SliderItem
                            key={item.key}
                            prev={item.prev}
                            active={item.active}
                            next={item.next}
                        ></SliderItem>
                    ))}
                </div>
                <div className="slider__dots">
                    {this.state.items.map((item) => (
                        <span
                            key={item.key}
                            className={item.active ? 'active' : ''}
                        ></span>
                    ))}
                </div>
            </section>
        )
    }
}
