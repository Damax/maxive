import React from 'react'

export type SliderItemProps = {
    active: boolean
    key: string | number
    prev?: number
    next?: number
}

export class SliderItem extends React.Component<SliderItemProps> {
    static defaultProps: SliderItemProps = {
        active: false,
        key: new Date().toString(),
    }

    private className() {
        let className: string = 'slider__item'
        if (this.props.active) {
            className += ' slider__item--active'
        } else if (this.props.prev) {
            className += ` slider__item--prev-${this.props.prev}`
        } else if (this.props.next) {
            className += ` slider__item--next-${this.props.next}`
        }

        return className
    }

    render(): React.ReactNode {
        return (
            <article className={this.className()}>
                {this.props.children}
            </article>
        )
    }
}
