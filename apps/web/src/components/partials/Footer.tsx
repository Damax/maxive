import React from 'react'

export class Footer extends React.Component {
    render(): React.ReactNode {
        return (
            <footer className="Footer">
                <div>
                    <p>Ce site est placé sous licence lobre AGPL+3</p>
                </div>
            </footer>
        )
    }
}
