import React from 'react'
import { Slider } from './utils/Slider'
export class MainSection extends React.Component {
    render(): React.ReactNode {
        return (
            <section className="MainSection">
                <Slider></Slider>
            </section>
        )
    }
}
