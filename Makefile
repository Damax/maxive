isDocker := $(shell docker info > /dev/null 2>&1 && echo 1)
isPodman := $(shell podman info > /dev/null 2>&1 && echo 1)

ifeq ($(isPodman), 1)
	dc := podman-compose
	de := $(dc) exec
	dr := $(dc) run --rm -d
	sy := $(dr) app pnpm
else
	ifeq ($(isDocker), 1)
		dc := docker-compose
		de := $(dc) exec
		dr := $(dc) run --rm -d
		sy := $(dr) app /bin/pnpm
	else 
		sy := pnpm
	endif
endif

.PHONY: install dev build migrate migration

install:
	$(sy) install
	make migrate

dev:
	$(dc) up -f compose.yml -f compose.dev.yml

build:
	$(sy) build -r 

build-docker: 
	$(dc) build -f compose.yml

test:
	$(sy) test -r

start: migration mgirate
	$(sy) start -r

migration:
	cd apps/api && $(sy) mikro-orm migration:create

migrate:
	cd apps/api && $(sy) mikro-orm migration:up

down:
	cd apps/api && $(sy) mikro-orm migration:down

lint:
	$(sy) format:lint -r