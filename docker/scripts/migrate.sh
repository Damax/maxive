#! /bin/sh

echo "-- Create migrations --"
pnpm migrations -r

echo "-- Successfuly created migrations --"

echo "-- Migrate database --"
pnpm migrate -r
echo "-- Successfuly migrated database --"
